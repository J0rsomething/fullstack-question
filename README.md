# Fullstack React Quiz

## Introduction

This is a simple quiz game implemented with React and Redux. All question data is stored locally at /src/constants.js. This quiz game supports various number of questions listed in constants.js. The routing logic implemented with React router v4 is completely safe when user tries to alter the link.

During the game, user is able to go back and forward between first question and newest question viewed. When last question answered, user will be able to see the result of the quiz.

## To run this application
Clone this repo to local and:
```
npm install
npm start
```
