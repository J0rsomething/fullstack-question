import React, {Component} from 'react'
import {connect} from 'react-redux'
import questions from '../../constants'
import {Redirect} from 'react-router-dom'
import {onQuestionAnswer} from '../../reducer/reducer'
import Question from '../../component/question/question'

const mapStateToProps = state => ({
  farest_answered: state.user_answers.length-1,
  user_answers:state.user_answers
})
const mapDispatchToProps = ({
  onQuestionAnswer
})

const QUIZ_LENGTH = questions.length

class QuestionContainer extends Component {
  constructor() {
    super()

    this.state = {
      //question index current showed
      current:0,
    }

    this._handleNext = this._handleNext.bind(this)
    this._handlePrevious = this._handlePrevious.bind(this)
    this._nextButton = this._nextButton.bind(this)
    this._previousButton = this._previousButton.bind(this)
  }


  //when NEXT button clicked
  _handleNext() {
    this.setState({
      current: this.state.current+1
    })
  }

  //when PREVIOUS button clicked
  _handlePrevious() {
    this.setState({
      current: this.state.current-1
    })
  }

  //decide NEXT button status
  _nextButton() {
    if(this.state.current !== QUIZ_LENGTH-1) {
      if(this.state.current <= this.props.farest_answered) {
        return <button onClick={this._handleNext}>Next</button>
      } else {
        return <button disabled>Next</button>
      }
    } else { //reach end of the quiz. will show result if clicked
      if(this.props.farest_answered < QUIZ_LENGTH-1){
        return <button disabled>Result</button>
      } else {
        return <button onClick={this._handleNext}>Result</button>
      }
    }
  }

  //decide PREVIOUS button status
  _previousButton() {
    if(this.state.current === 0) {
      return <button disabled>Previous</button>
    } else {
      return <button onClick={this._handlePrevious}>Previous</button>
    }
  }

  render() {
    const QuestionArray = questions.map((item, index)=>
      <Question
        index={index}
        item={item}
        user_answers={this.props.user_answers}
        current={this.state.current}
        onQuestionAnswer={this.props.onQuestionAnswer}/>
    )

    return(
      <div className='container'>
        <div className='header'>
          Question #{this.state.current+1}
        </div>
        <div className='content'>
          {
            this.state.current < QUIZ_LENGTH?
              QuestionArray[this.state.current]:
              <Redirect to='/result' />
          }
        </div>
        <div className='footer question_footer'>
          {
            this._previousButton()
          }
          {
            this._nextButton()
          }
        </div>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionContainer)
