const questions = [
  {
    category: 'Movie',
    question: 'The Titanic had enough lifeboats to save almost everyone on board.',
    answer: 'False'
  },
  {
    category: 'Geography',
    question: 'The smallest country in the world is Monaco.',
    answer: 'False'
  },
  {
    category: 'Biology',
    question: 'The biggest muscle in the human body is the gluteus maximus (buttock muscle).',
    answer: 'True'
  },
  {
    category: 'Movie',
    question: 'Bollywood is the nickname of Britain\'s movie industry.',
    answer: 'False'
  },
  {
    category: 'Movie',
    question: 'When Mickey Mouse debuted on screen in 1928, it was in a silent film.',
    answer: 'False'
  },
  {
    category: 'History',
    question: 'Only Americans and Soviets have walked on the Moon.',
    answer: 'False'
  },
  {
    category: 'Geometry',
    question: 'A heptagon has six sides.',
    answer: 'False'
  },
  {
    category: 'Biology',
    question: 'Vampire bats feed on blood.',
    answer: 'True'
  },
  {
    category: 'Science',
    question: 'Lightning is seen before it\'s heard because light travels faster than sound.',
    answer: 'True'
  },
  {
    category: 'Science',
    question: 'A penny dropped from a tall building can reach sufficient velocity to kill a pedestrian below.',
    answer: 'False'
  }
]
export default questions
