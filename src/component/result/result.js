import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {onStartAgain} from '../../reducer/reducer'
import questions from '../../constants'
import Transcript from '../transcript/transcript'


const mapStateToProps = state => ({
  ...state
})
const mapDispatchToProps = ({
  onStartAgain
})


class Result extends React.Component {
  constructor() {
    super()
    this._countCorrect = this._countCorrect.bind(this)
  }

  componentWillUnmount() {
    this.props.onStartAgain()
    this.props.history.replace('/')
  }

  _countCorrect() {
    let counter = 0
    questions.forEach((item, index)=>{
      if(item.answer === this.props.user_answers[index]) {
        counter++
      }
    })
    return counter
  }

  render() {
    return(
      <div className='container'>
        {
          this.props.user_answers.length !==0? null:<Redirect to='/' />
        }
        <div className='header'>
          Result
        </div>
        <div className='content'>
          <h2>{`Your score was ${this._countCorrect()}/${questions.length}`}</h2>
          <h3 style={{marginBottom: '0px'}}>Detailed Results</h3>
          <Transcript
            questions={questions}
            answers={this.props.user_answers}/>
        </div>
        <div className='footer'>
          <button onClick={()=>{
            this.props.onStartAgain()
            this.props.history.replace('/')
          }}>Play Again</button>
        </div>
      </div>
    )
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Result)
