import React from 'react'
import questions from '../../constants'
const Introduction = ({history,startAgain}) => (
  <div className='container'>
    <div className='header'>
      Introduction
    </div>
    <div className='content'>
      <h2>Welcome to the Trivia Challenge!</h2>
      <h2>{`You will be presented with ${questions.length} True or False Questions`}</h2>
      <h2>Can you score 100%?</h2>
    </div>
    <div className='footer'>
      <button onClick={()=>{history.push('/question')}}>Start Quiz</button>
    </div>
  </div>
)
export default Introduction
