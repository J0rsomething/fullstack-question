const styles = {
  detail_list: {
    listStyle: 'none',
    padding: '0px',
    fontSize: '12px'
  },
  detail_item: {
    borderTop: '1px solid #cccccc'
  },
  detail_answer: {
    color: '#999999',
    fontSize: '10px',
    margin: '0px 0px 10px 0px'
  },
  detail_icon_wrong: {
    right: '0px',
    color: '#ff5050',
    float: 'right'
  },
  detail_icon_correct: {
    right: '0px',
    color: '#00cc66',
    float: 'right'
  }

}

export default styles
