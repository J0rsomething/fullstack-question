import React from 'react'
import styles from './styles'
const Transcript = ({questions, answers}) => (
  <ul style={styles.detail_list}>
    {
      questions.map((item, index) => {
        let result = item.answer === answers[index]
        return (
            <li key={index} style={styles.detail_item}>
              <p style={{margin: '10px 0px 0px 0px'}}>{`${index+1}. ${item.question}`}</p>
              <p style={styles.detail_answer}>{`Your answer: "${answers[index]}", correct answer: "${item.answer}"`}
                <span
                  style={result?styles.detail_icon_correct:styles.detail_icon_wrong}>{result ? 'Correct' : 'Wrong'}</span>
              </p>
            </li>
        )
      })
    }
  </ul>
)

export default Transcript
