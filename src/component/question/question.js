import React from 'react'
import styles from './styles'

const Question = ({item, user_answers, current, onQuestionAnswer, index}) => (
  <div className='question_pane'>
    <h2>{item.category}: </h2>
    <h2>{item.question}</h2>
    <div className='button_bar'>
      <button
        style={user_answers&&user_answers[current]==='True'?styles.button_answered:{}}
        onClick={()=>onQuestionAnswer({index, answer: 'True'})}>
        True
      </button>
      <button
        style={user_answers&&user_answers[current]==='False'?styles.button_answered:{}}
        onClick={()=>onQuestionAnswer({index, answer: 'False'})}>
        False
      </button>
    </div>
  </div>
)
export default Question
